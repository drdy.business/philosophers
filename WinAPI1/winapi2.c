#include "pch.h"

CRITICAL_SECTION ghCritialSection;

#define NUM_OF_PHILOSOPHES 5
#define MAX_READ_SIZE 6
#define MAX_WRITE_SIZE 6

DWORD WINAPI thread(LPVOID params) {
	return *(int*)params;
}

DWORD WINAPI philosopher(LPVOID params) {
	LPINT sticks = ((LPINT)params)[1];
	for (int i = 0; i < 1000000; ++i) {

	}
}

DWORD main() {
	/*
	int values[] = { 1, 2, 3, 4 };
	HANDLE hThreads[4] = { 0 };

	for (int i = 0; i < 4; ++i) {
		hThreads[i] = CreateThread(NULL, 0, thread, values + i, 0, NULL);
	}
	WaitForMultipleObjects(4, hThreads, TRUE, INFINITE);

	DWORD returnValues[4] = { 0 };

	for (int i = 0; i < 4; ++i) {
		GetExitCodeThread(hThreads[i], returnValues + i);
		printf("%d\n", returnValues[i]);
	}
	*/
	/**************************/
	/*
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	HANDLE hProcess = CreateProcessW(L"test.exe", 
									 L"first second", 
									 NULL,
									 NULL, 
									 FALSE,
									 NORMAL_PRIORITY_CLASS,
									 NULL,
									 L"C:/Users/root/source/repos/WinAPI1/WinAPI1",
									 &si,
									 &pi);
	if (!hProcess)
		return 1;

	WaitForSingleObject(hProcess, INFINITE);
	*/

	//InitializeCriticalSection(&ghCritialSection);

	HANDLE philosophes[NUM_OF_PHILOSOPHES] = { 0 };
	HANDLE glMutex = CreateMutexW(0, TRUE, L"sticks");

	if (!glMutex) {
		printf("An error has occured while creating the mutex: %d\n", GetLastError());
	}

	HANDLE hWriteFile = CreateFile(  // Creating a file with write permissions
		L"sticks.txt",
		GENERIC_WRITE,
		0,
		NULL,
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL
	);

	if (hWriteFile == INVALID_HANDLE_VALUE) {  // If failure
		printf("Couldn't open: %d\n", GetLastError());
		_exit(1);
	}

	CHAR lpBuffer[] = "11111";
	WaitForSingleObject(glMutex, INFINITE);
	BOOL success = WriteFile(hWriteFile, lpBuffer, sizeof(lpBuffer), NULL, NULL);

	if (!success) {
		printf("Couldn't write: %d\n", GetLastError());
		_exit(1);
	}

	CloseHandle(hWriteFile);
	ReleaseMutex(glMutex);

	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));
	WCHAR param[] = L"0";

	for (int i = 0; i < NUM_OF_PHILOSOPHES; ++i) {
		param[0] = (i + 48);
		philosophes[i] = CreateProcessW(L"philosoph.exe",
										param,
										NULL,
										NULL,
										FALSE,
										NORMAL_PRIORITY_CLASS,
										NULL,
										L"C:/Users/root/source/repos/WinAPI1/WinAPI1",
										&si,
										&pi);

		//philosophes[i] = CreateThread(NULL, 0, philosopher, param, 0, NULL);

		if (!philosophes[i])
			return 1;

	}
	WaitForMultipleObjects(4, philosophes, TRUE, INFINITE);
	WaitForSingleObject(glMutex, INFINITE);
	CloseHandle(glMutex);
	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);
	return 0;
}