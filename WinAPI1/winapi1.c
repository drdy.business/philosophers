#include "pch.h"

#define MAX_READ_SIZE 512

int winapi1() {

	HANDLE hFile = CreateFile(
		L"test.txt", 
		GENERIC_READ, 
		0, 
		NULL, 
		OPEN_ALWAYS, 
		FILE_ATTRIBUTE_READONLY, 
		NULL
	);

	if (hFile == INVALID_HANDLE_VALUE) {
		printf("Error code: %d\n", GetLastError());
		_exit(1);
	}

	LPVOID lpBuffer[MAX_READ_SIZE] = { 0 };

	BOOL success = ReadFile(hFile, lpBuffer, MAX_READ_SIZE, NULL, NULL);

	if (!success) {
		printf("Error code: %d\n", GetLastError());
		_exit(1);
	}

	printf("Contents of the file:\n%s\n", (PCHAR)lpBuffer);

	CloseHandle(hFile);
}