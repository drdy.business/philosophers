#include <stdio.h>
#include <windows.h>
#include <time.h>

#define MAX_WRITE_SIZE 16
#define MAX_READ_SIZE 16

DWORD main(const DWORD argc, LPCWSTR* argv) {
	printf("%s\n", argv[0]);
	printf("I'm philosophe #%d!\n", atoi((LPCSTR)argv[0]) + 1);
	time_t now;
	time(&now);
	DWORD serialNum = atoi((LPCSTR)argv[0]);
	HANDLE glMutex;
	glMutex = CreateMutexW(0, FALSE, L"sticks");

	if (glMutex == NULL) {
		printf("No mutex %d\n", serialNum + 1);
		return 1;
	}

	for (int i = 0; i < 10; ++i) {
		WaitForSingleObject(glMutex, INFINITE);
		//printf("Joined %d\n", serialNum + 1);
		HANDLE hReadFile = CreateFileW(  // Creating a file with read permissions
			L"sticks.txt",
			GENERIC_READ,
			0,
			NULL,
			OPEN_ALWAYS,
			FILE_ATTRIBUTE_READONLY,
			NULL
		);

		if (hReadFile == INVALID_HANDLE_VALUE) {  // If failure
			printf("Couldnt open: %d\n", GetLastError());
			_exit(1);
		}

		CHAR lpBuffer[MAX_READ_SIZE] = { 0 };

		BOOL success = ReadFile(hReadFile, lpBuffer, MAX_READ_SIZE, NULL, NULL);  // Reading the file into the buffer

		if (!success) {
			printf("Couldn't read: %d\n", GetLastError());
			_exit(1);
		}

		CloseHandle(hReadFile);
		ReleaseMutex(glMutex);
		//printf("1: Released mutex %d\n", serialNum + 1);
		//printf("SER = %d, buf = %s\n", serialNum + 1, lpBuffer);

		if (lpBuffer[serialNum] - '0' && lpBuffer[serialNum + (serialNum == 4 ? -serialNum : 1)] - '0') {
			//printf("Joined if %d\n", serialNum + 1);

			WaitForSingleObject(glMutex, INFINITE);
			if (!DeleteFileW(L"sticks.txt")) {
				printf("Error while deleting: %d\n", GetLastError());
				return 1;
			}

			HANDLE hWriteFile = CreateFileW(  // Creating a file with write permissions
				L"sticks.txt",
				GENERIC_WRITE,
				0,
				NULL,
				OPEN_ALWAYS,
				FILE_ATTRIBUTE_NORMAL,
				NULL
			);

			if (hWriteFile == INVALID_HANDLE_VALUE) {  // If failure
				printf("Couldn't open: %d\n", GetLastError());
				_exit(1);
			}

			lpBuffer[serialNum] = (CHAR)'0';
			lpBuffer[serialNum + (serialNum == 4 ? -serialNum : 1)] = (CHAR)'0';

			success = WriteFile(hWriteFile, lpBuffer, sizeof(lpBuffer), NULL, NULL);

			if (!success) {
				printf("Couldn't write: %d\n", GetLastError());
				_exit(1);
			}

			//printf("Philosopher #%d: Is eating\n", serialNum + 1);

			CloseHandle(hWriteFile);
			ReleaseMutex(glMutex);
			//printf("3: Released mutex %d\n", serialNum + 1);
			
			lpBuffer[serialNum] = (CHAR)'1';
			lpBuffer[serialNum + (serialNum == 4 ? -serialNum : 1)] = (CHAR)'1';

			WaitForSingleObject(glMutex, INFINITE);
			hWriteFile = CreateFileW(  // Creating a file with write permissions
				L"sticks.txt",
				GENERIC_WRITE,
				0,
				NULL,
				OPEN_ALWAYS,
				FILE_ATTRIBUTE_NORMAL,
				NULL
			);

			if (hWriteFile == INVALID_HANDLE_VALUE) {  // If failure
				printf("Couldn't open: %d\n", GetLastError());
				_exit(1);
			}


			success = WriteFile(hWriteFile, lpBuffer, MAX_WRITE_SIZE, NULL, NULL);

			if (!success) {
				printf("Couldn't write: %d\n", GetLastError());
				_exit(1);
			}

			//printf("Philosopher #%d: Ate\n", serialNum + 1);
			//printf("SER = %d, buf = %s\n", serialNum + 1, lpBuffer);

			CloseHandle(hWriteFile);
			ReleaseMutex(glMutex);
			//printf("3: Released mutex %d\n", serialNum + 1);
		}
	}
	time_t end;
	time(&end);
	printf("It took philophisor #%d: %f seconds!\n", serialNum + 1, difftime(now, end));
	return 0;
}