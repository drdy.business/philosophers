#include "pch.h"
#include <ctime>

#define NUM_OF_PHILOSOPHERS 5

CRITICAL_SECTION cs;

CHAR sticks[] = "11111";

DWORD WINAPI philosopher(LPVOID params) {

	int serialNum = *(int*)params;
	printf("Philosopher #%d joined\n", serialNum + 1);

	time_t now;
	time(&now);
	for (int i = 0; i < 1000000; ++i) {
		if (sticks[serialNum] - '0' && sticks[serialNum + (serialNum == 4 ? -serialNum : 1)] - '0') {

			EnterCriticalSection(&cs);
			sticks[serialNum] = '0';
			sticks[serialNum + (serialNum == 4 ? -serialNum : 1)] = '0';
			LeaveCriticalSection(&cs);
			//printf("Philosopher #%d is eating\n", serialNum + 1);


			EnterCriticalSection(&cs);
			sticks[serialNum] = '1';
			sticks[serialNum + (serialNum == 4 ? -serialNum : 1)] = '1';
			LeaveCriticalSection(&cs);
			//printf("Philosopher #%d ate\n", serialNum + 1);
		}
	}
	time_t end;
	time(&end);
	printf("It took philophisor #%d: %f seconds!\n", serialNum + 1, difftime(now, end));
	return 0;
}


int main4() {

	InitializeCriticalSection(&cs);

	int values[] = { 0, 1, 2, 3, 4 };
	HANDLE hThreads[NUM_OF_PHILOSOPHERS] = { 0 };

	for (int i = 0; i < NUM_OF_PHILOSOPHERS; ++i) {
		hThreads[i] = CreateThread(NULL, 0, philosopher, values + i, 0, NULL);
	}
	WaitForMultipleObjects(5, hThreads, TRUE, INFINITE);

	return 0;
}
