#include "pch.h"
#include <string>

std::string to_utf8(const std::wstring& s)
{
	/*
	wstring_convert<codecvt_utf8_utf16<wchar_t>> utf16conv;
	return utf16conv.to_bytes(s);
	*/

	std::string utf8;
	int len = WideCharToMultiByte(CP_UTF8, 0, s.c_str(), s.length(), NULL, 0, NULL, NULL);
	if (len > 0)
	{
		utf8.resize(len);
		WideCharToMultiByte(CP_UTF8, 0, s.c_str(), s.length(), &utf8[0], len, NULL, NULL);
	}
	return utf8;
}

int main2() {

	if (!DeleteFileW(L"sticks.txt")) {
		printf("Error while deleting!\n");
		return 1;
	}

	HANDLE hWriteFile = CreateFile(  // Creating a file with write permissions
		L"sticks.txt",
		GENERIC_WRITE,
		0,
		NULL,
		OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL
	);

	if (hWriteFile == INVALID_HANDLE_VALUE) {  // If failure
		printf("Couldn't open: %d\n", GetLastError());
		_exit(1);
	}


	std::wstring buf = L"10011";
	std::string lpBuffer = to_utf8(buf);

	BOOL success = WriteFile(hWriteFile, lpBuffer.c_str(), lpBuffer.size(), NULL, NULL);

	if (!success) {
		printf("Couldn't write: %d\n", GetLastError());
		_exit(1);
	}

	CloseHandle(hWriteFile);
	return 0;
}
